package model;

import util.Util;

import java.util.function.DoubleUnaryOperator;

public class Neuron {
	protected double[] weights;
	protected final double learningRate;
	protected double outputCache;
	protected double delta;
	protected final DoubleUnaryOperator activationFunction;
	protected final DoubleUnaryOperator derivativeActivationFunction;

	public Neuron(double[] weights, double learningRate, DoubleUnaryOperator activationFunction,
			DoubleUnaryOperator derivativeActivationFunction) {
		this.weights = weights;
		this.learningRate = learningRate;
		outputCache = 0.0;
		delta = 0.0;
		this.activationFunction = activationFunction;
		this.derivativeActivationFunction = derivativeActivationFunction;
	}

	public double output(double[] inputs) {
		outputCache = Util.dotProduct(inputs, weights);
		return activationFunction.applyAsDouble(outputCache);
	}

}
